variable "github_token" {
  type = string
  description = "PAT Token for the account"
}

variable "genereted_repo_name" {
  type = string
  description = "Name of the future repository "
}